# atlaskit-bot

## Dev
###First timers
* Create an app on https://developer.atlassian.com/apps
* Enable the "Stride API" for that particular app.
* Install the Stride app to your Stride instance using the provided Installation URL.

### Running locally
* Set the client_id and client_secret from the apps settings page by running `CLIENT_ID=xxx CLIENT_SECRET=yyy` (or add to your `~/.profile`)
* `yarn run dev` will start an instance at port 8080
* `ngrok http 8080` (which will make your local publicly available)
* Paste the ngrok base https url + `/descriptor` into Descriptor URL page on the app install page https://developer.atlassian.com/apps (example: `https://8eae8f50.ngrok.io/descriptor`)

### Deploying to dev instance
* `yarn run dev:deploy`

## Running on Micros (Atlassian)
* Note: Make sure you set your CLIENT_ID and CLIENT_SECRET on your micros env *before* you deploy your service
`micros stash:set -s bot-service-name CLIENT_ID xxx`
`micros stash:set -s bot-service-name CLIENT_SECRET xxx`

* Get Jony to give you access to both `dev` and `prod` micros instance

##Prod
* `yarn run prod:deploy` (make sure you have access)


Other commands

See `package.json`
