FROM node:7-alpine

COPY . /opt/service
WORKDIR /opt/service
RUN npm install
EXPOSE 8080

CMD npm start