const { Document, marks, sub } = require('adf-builder');

const MENTION_RESPONSE = "To see me in action, just mention @disturbed with a question. e.g. Where is the doc for @atlaskit/icon?";
const INSTALL_RESPONSE = `Thank you for installing the @atlaskit bot. ${MENTION_RESPONSE}`;
const DEFAULT_LETUSKNOW = `\n\nLet us know if this is what you need. Someone will reply to you shortly.`;
const TURNAROUND_RESPONSE = `\nNote: We aim to respond to you within an hour during Sydney business hours. There is no @disturbed role on rotation.`;

const CONTRIBUTION_RESPONSE = new Document();
CONTRIBUTION_RESPONSE.panel("info").paragraph()
.text("It looks like you are looking for information about contribution. Here is our ")
.link("contributing guide", "http://ak-mk-2-prod.netlify.com/docs/guides/contributing")
.text('.')
.text(DEFAULT_LETUSKNOW)
.text(TURNAROUND_RESPONSE, marks().sub().em());

const getDocResponse = function (pkgName, pkgUrl){
  var doc = new Document();
  (
    (pkgName)?
      doc.panel("info").paragraph()
      .text(`It looks like you are looking for '${pkgName}' documentation. Please find it `)
      .link('here' , pkgUrl)
      .text('.')
    :
      doc.panel("info").paragraph()
      .text('It looks like you are looking for documentation of our components. Please find them at ')
      .link(pkgUrl , pkgUrl)
      .text('.')
  ).text(DEFAULT_LETUSKNOW)
  .text(TURNAROUND_RESPONSE, marks().sub().em());
  return doc;
}

const ROADMAP_RESPONSE = new Document();
ROADMAP_RESPONSE.panel("info").paragraph()
  .text("It looks like you are looking for our roadmap. Here is our ")
  .link("roadmap trello board", "https://trello.com/b/671gxxu8/adg-atlaskit-roadmap")
  .text('.')
  .text(DEFAULT_LETUSKNOW)
  .text(TURNAROUND_RESPONSE, marks().sub().em());

const GENERAL_RESPONSE = new Document();
GENERAL_RESPONSE.panel("info").paragraph()
.strong("Thanks for the query. We will be with you shortly.")
.text("\n\nIf it is regarding a bug or improvement, please go to the \'Atlaskit requests\' Stride room. ")
.text("It is possible that the issue has been already raised, please do a cursory search in our ")
.link("Jira project", "https://ecosystem.atlassian.net/projects/AK/issues")
.text(TURNAROUND_RESPONSE, marks().sub().em())

module.exports = {
  getDocResponse:         getDocResponse,
  GENERAL_RESPONSE:       GENERAL_RESPONSE,
  ROADMAP_RESPONSE:       ROADMAP_RESPONSE,
  CONTRIBUTION_RESPONSE:  CONTRIBUTION_RESPONSE,
  MENTION_RESPONSE:       MENTION_RESPONSE,
  INSTALL_RESPONSE:       INSTALL_RESPONSE
}