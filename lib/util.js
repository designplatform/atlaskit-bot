var { Document } = require('adf-builder');
var fetch = require('node-fetch');
var { GENERAL_RESPONSE, ROADMAP_RESPONSE, CONTRIBUTION_RESPONSE, getDocResponse } = require('./messages');


var simpleMessage = function (msg){
  //returns the ADF array with just the text
  if(msg && typeof(msg) === 'string')
  {
    const doc = new Document();
    doc.panel("info").paragraph().text(msg);
    return doc.toJSON();
  }
  else {
    throw 'the parameter has to be a string'
  }
}
//TODO: How do we handle two packages?
function getPkgNameFromMsg(text){
  var componentNameRegex =  [
                              new RegExp(/@atlaskit\/([\w-_]*)/gi), 
                              new RegExp(/(\w+) component/gi)
                            ],
      i = 0,
      regexresult = null;

  while (!regexresult && componentNameRegex[i]){
    regexresult = componentNameRegex[i].exec(text);
    i++;
  }

  return regexresult&&regexresult[1].toLowerCase() || '';
}
//TODO: how do we handle if the package does not exist? It is unlikely?
async function getPackage (pkg) {
  // Need to be updated with mk-2 as almost all packages have been migrated
  var url = 'https://atlaskit.atlassian.com/components/';

  if (!pkg) return null;
  var pkgJson = await fetch(`https://unpkg.com/@atlaskit/${pkg}/package.json`)
                      .then(r => r.json())
                      .catch(()=>{return null;});
  if (!pkgJson){
    return null
  }else if (pkgJson.repository.url){
    urlPkg =  pkgJson.repository.url;
  }
  else {
    urlPkg =  pkgJson.repository;
  }
  
  return url = ( (urlPkg.includes('atlaskit-mk2') || urlPkg.includes('atlaskit-mk-2'))
  ? 'https://ak-mk-2-prod.netlify.com/mk-2/packages/elements/'
  : 'https://atlaskit.atlassian.com/components/')+pkg;

};

async function processQuestions(text){
  let   doc = new Document();
  const docslocation_regex = /(where is|docs|how do I|doc|documentation|examples|example|\w+ component)/gi,
        contribution_regex = /(contribute|contribution|contributed|contributing|Pull Request|\w pr|pr )/gi,
        roadmap_regex = /(when is|roadmap|when will|timeline)/gi;

    if (contribution_regex.test(text)){
      // This person is trying to out how to contribute
      doc = CONTRIBUTION_RESPONSE;
      console.log('atlaskit bot responded with Contribution info');
    } else if (roadmap_regex.test(text)){
      // This person is trying to out about timelines
      doc = ROADMAP_RESPONSE;
      console.log('atlaskit bot responded with Roadmap info');
    } else if (docslocation_regex.test(text)){
      // This person is trying to find docs
      var pkgName = getPkgNameFromMsg(text);
      var pkgUrl = await getPackage(pkgName);
      if (pkgUrl === null){
        pkgUrl = 'https://atlaskit.atlassian.com/components/';
        pkgName = ''
      }
      doc = getDocResponse(pkgName, pkgUrl);
      (pkgName !=='')?
      console.log(`atlaskit bot responded with ${pkgName} Documentation info`):
      console.log('atlaskit bot responded with general Documentation.' );

  } else {
    // Everything else
    doc = GENERAL_RESPONSE;
    console.log('atlaskit bot responded with General info');
  }
  return doc.toJSON()
}
module.exports = {
  simpleMessage: simpleMessage,
  getPackage: getPackage,
  processQuestions: processQuestions,
  getPkgNameFromMsg: getPkgNameFromMsg
};