var PORT = process.env.PORT || 8080;
var CLIENT_ID = process.env.CLIENT_ID; 
var CLIENT_SECRET = process.env.CLIENT_SECRET;
var API_BASE_URL = 'https://api.atlassian.com';

var logger = require('bunyan').createLogger({
  name: 'atlaskit-bot-log',
  level: 'info'
});
console.error = logger.error.bind(logger);
console.log = logger.info.bind(logger);
console.warn = logger.warn.bind(logger);

if (!PORT || !CLIENT_ID || !CLIENT_SECRET) {
  console.log ("Usage:");
  console.log("PORT=<http port> CLIENT_ID=<app client ID> CLIENT_SECRET=<app client secret> node app.js");
  process.exit();
}

var _ = require('lodash');
var fs = require('fs');
var express = require('express');
var bodyParser = require('body-parser');
var http = require('http');
var request = require('request');
var app = express();
var fetch = require('node-fetch');
var { simpleMessage, processQuestions } = require('./lib/util');
var { MENTION_RESPONSE, INSTALL_RESPONSE } = require('./lib/messages');
app.use(bodyParser.json());
app.use(express.static('.'));

function getAccessToken(callback) {
  var options = {
    uri: 'https://auth.atlassian.com/oauth/token',
    method: 'POST',
    json: {
      grant_type: "client_credentials",
      client_id: CLIENT_ID,
      client_secret: CLIENT_SECRET,
      "audience": "api.atlassian.com"
    }
  };
  request(options, function (err, response, body) {
    if (response.statusCode === 200 && body.access_token) {
      callback(null, body.access_token);
    } else {
      callback("could not generate access token: " + JSON.stringify(response));
    }
  });
}

function sendMessage(cloudId, conversationId, documentObject, callback) {
  getAccessToken(function (err, accessToken) {
    if (err) {
      callback(err);
    } else {
      var uri = API_BASE_URL + '/site/' + cloudId + '/conversation/' + conversationId + '/message';
      var options = {
        uri: uri,
        method: 'POST',
        headers: {
          authorization: "Bearer " + accessToken,
          "cache-control": "no-cache"
        },
        json: {
          body: documentObject
        }
      }

      request(options, function (err, response, body) {
          callback(err, body);
      });
    }
  });
}

function sendReply(message, replyObject, callback) {
  var cloudId = message.cloudId;
  var conversationId = message.conversation.id;
  var userId = message.sender.id;

  sendMessage(cloudId, conversationId, replyObject, function (err, response) {
    if (err) {
      console.log ('Error sending message: ' + err);
      callback(err);
    } else {
      callback(null, response);
    }
  });
}

app.post('/installed',
    function (req, res) {
      console.log(`atlaskit-bot /installed`);
      var cloudId = req.body.cloudId;
      var conversationId = req.body.resourceId;
      sendMessage(cloudId, conversationId, simpleMessage(INSTALL_RESPONSE), function(err, response){
        if(err)
          console.log(err);
      });
      res.sendStatus(204);
      res.end('OK');
    }
);
app.post('/uninstalled',
    function (req, res) {
      console.log(`atlaskit-bot /uninstalled`); // Can't reach back to conversation so skip sending message
      res.end('OK');
});

app.post('/bot-mention',
    async function (req, res) {
      console.log(`atlaskit-bot /bot-mention with: "${req.body.message.text}" in the "${req.body.conversation.name}" room`);
      sendReply(req.body, simpleMessage(MENTION_RESPONSE), function (err, response) {
        if (err) {
          console.log(err);
          res.sendStatus(500);
        } else {
          res.sendStatus(204);
          res.end('OK');
        }
      });
});
app.post('/bot-message',
    async function (req, res) {
      console.log(`atlaskit-bot /bot-message question: "${req.body.message.text}" in the "${req.body.conversation.name}" room`);
      sendReply(req.body, await processQuestions(req.body.message.text), function (err, response) {
        if (err) {
          console.log(err);
          res.sendStatus(500);
        } else {
          res.sendStatus(204);
          res.end('OK');
        }
      });
    }
);
/**
 * Don't worry about this for now.
 */
app.get('/descriptor', function (req, res) {
  console.log(`atlaskit-bot /descriptor`);
  fs.readFile('./app-descriptor.json', function (err, descriptorTemplate) {
    var template = _.template(descriptorTemplate);
    var descriptor = template({
      host: 'https://' + req.headers.host
    });
    res.set('Content-Type', 'application/json');
    res.send(descriptor);
  });
});

app.get('/healthcheck', function (req, res) {
  console.log(`atlaskit-bot /healthcheck`);
  res.sendStatus(200);
  res.end('OK');
});

http.createServer(app).listen(PORT, function () {
  console.log('App running on port ' + PORT);
});

module.exports = app;