process.env.CLIENT_ID = 'xxx';
process.env.CLIENT_SECRET = 'yyy';
process.env.NODE_ENV = 'test';

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();

chai.use(chaiHttp);

/*
* HealthCheck
*/
describe('/GET healthcheck', () => {
    it('it should GET healthcheck with a 200', (done) => {
      chai.request(server)
          .get('/healthcheck')
          .end((err, res) => {
              res.should.have.status(200);
              res.text.should.equal("OK")
            done();
          });
    });
});
