var {   simpleMessage, 
        getPackage, 
        processQuestions, 
        getPkgNameFromMsg 
    } = require('../lib/util');
assert = require('chai').assert;
should = require('chai').should();
var { Document } = require('adf-builder');
var { GENERAL_RESPONSE, ROADMAP_RESPONSE, CONTRIBUTION_RESPONSE, getDocResponse } = require('../lib/messages');

describe('simpleMessage()', () => {
    var doc = new Document();
    doc.panel("info").paragraph().text('ok')
    it('should return the simple ADF json object', () => {
        assert.deepEqual(typeof(simpleMessage('ok')),'object');
    });
    it('should throw an error if not passing a string', () => {
        var ok = {};
        assert.throws(function() {simpleMessage(ok)},'the parameter has to be a string');
    });
    it('should return the document with the correct msg', () => {
        assert.deepEqual(doc.toJSON(),simpleMessage('ok'));
    });
    it('should return the document with the incorrect msg', () => {
        (doc.toJSON()).should.not.equal('hello');
    });
});

describe('getPkgNameFromMsg()', () => {
    // Those two arrays have to be improved
    var arrFind = 
    {
        'where the component @atlaskit/field-text?': 'field-text',
        'the @atlaskit/i component': 'i',
        '@disturbed @atlaskit/icon': 'icon',
    };
    for(var key in arrFind) {
        it('should find the package ' + key, () => {
            assert.equal(getPkgNameFromMsg(key), arrFind[key]);
        });
    };

});

describe('getPackage()', () => {
    it('should return the overview url if no packages', async () => {
        assert.equal(await getPackage(), null);
    });
    it('should return the overview url if package does not exist',async () => {
        assert.equal(await getPackage('hello'), null);
    });
    it('should return the package url',async () => {
        assert.equal(await getPackage('button'), 'https://ak-mk-2-prod.netlify.com/mk-2/packages/elements/button');
    });
});

//TO REVIEW: Review scenarios for regex
describe('processQuestions()', () => {
    var doc = new Document();
    var expectedContribution = CONTRIBUTION_RESPONSE;

    var expectedRoadmap = ROADMAP_RESPONSE;
  
    var expectedLocationPackage = getDocResponse('button', 'https://ak-mk-2-prod.netlify.com/mk-2/packages/elements/button');

    var expectedLocationNoPackage = getDocResponse(null,'https://atlaskit.atlassian.com/components/');
    
    var expectedDefault = GENERAL_RESPONSE;

    var arrContribution = 
    [
        'who contribute?',
        'did you do this contribution?',
        'Have you ever contributed',
        'Can I submit a pull request',
        'do you have any pr?'
    ];

    var arrRoadmap = 
    [
        'when is?',
        'Is this component on your roadmap?',
        'When will React 16 update be done?',
        'What is the timeline for this component?'
    ];

    var arrLocationWithPackages = 
    [
        'Where is doc for the @atlaskit/button?',
        'How do I use @atlaskit/button?',
        'Where can I find the examples for @atlaskit/button?',
        'Where can I find the examples for button component?',
        'button component docs?'
    ];

    var arrLocationWithoutPackages = 
    [
        'Where is doc for the @atlaskit/x?'
    ];

    var arrDefault = 
    [
        'How can I get some help?',
        'Did you get the ticket I raised?'
    ];

    arrContribution.forEach((text) => {
        it(`should return the contribution ${text}`, async () => {
            var result = await processQuestions(text);
            assert.deepEqual(result ,expectedContribution.toJSON());
        });
    });
    arrRoadmap.forEach((text) => {
        it(`should return the roadmap ${text}`, async () => {
            var result = await processQuestions(text);
            assert.deepEqual(result, expectedRoadmap.toJSON());
        });
    });
    arrLocationWithPackages.forEach((text) => {
        it(`should return the roadmap ${text}`, async () => {
            var result = await processQuestions(text);
            assert.deepEqual(result, expectedLocationPackage.toJSON());
        });
    });
    arrLocationWithoutPackages.forEach((text) => {
        it('should return the doc location without packages ' + text,async () => {
            var result = await processQuestions(text);
            assert.deepEqual(result, expectedLocationNoPackage.toJSON());
        });
    });

    arrDefault.forEach((text) => {
        it('should return the default message ' + text,async () => {
            var result = await processQuestions(text);
            assert.deepEqual(result, expectedDefault.toJSON());
        });
    });
});